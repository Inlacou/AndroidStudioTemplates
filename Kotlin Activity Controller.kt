#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Intent
import com.github.salomonbrys.kotson.fromJson
import timber.log.Timber

#parse("File Header.java")
class ${NAME}(val view: ${VIEW}, val model: ${MODEL}) : BaseActCtrl(view, model) {
	
	init { //initialize
		
	}
	
}
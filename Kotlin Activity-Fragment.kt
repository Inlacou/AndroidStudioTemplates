#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import butterknife.BindView

import com.google.gson.Gson

import com.github.salomonbrys.kotson.fromJson

#parse("File Header.java")
class ${NAME} : BaseActivity() {

	@BindView(R.id.title) @JvmField var tvTitle: TextView? = null

	private var fragment: ${FragmentClass}? = null
	private lateinit var model: ${FragmentModel}

	companion object {
		fun navigate(activity: Activity, model: ${FragmentModel}) {
			val intent = Intent(activity, ${NAME}::class.java)

			intent.putExtra("model", Gson().toJson(model))

			activity.startActivity(intent)
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_fragment)

		getIntentData()

		initialize(savedInstanceState)

		configureActionBar()

		populate()
	}

	private fun getIntentData() {
		if (intent.hasExtra("model")) model = Gson().fromJson(intent.extras.getString("model"))
	}

	private fun configureActionBar() {
		val toolbar = findViewById<Toolbar>(R.id.toolbar)
		if (toolbar != null) {
			setSupportActionBar(toolbar)
			supportActionBar?.title = ""
			supportActionBar?.setDisplayHomeAsUpEnabled(true)
		}
	}

	private fun populate() {
		fragment = ${FragmentClass}.create(model)
		tvTitle?.text = ${FragmentClass}.getTitle(model)
		val fragmentManager = supportFragmentManager
		fragmentManager.beginTransaction()
				.replace(R.id.frame, fragment)
				.commit()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.none, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			android.R.id.home -> {
				// app icon in action bar clicked; goto parent activity.
				this.finish()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}
}
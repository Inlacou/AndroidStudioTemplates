#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

#parse("File Header.java")
class ${NAME}(private val itemList: MutableList<${MODEL}>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    
    private var lastAdded: Int = 0

    override fun getItemViewType(position: Int): Int {
        // Return type
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                val layoutView = LayoutInflater.from(parent.context).inflate(
                        R.layout.${LAYOUT_FILE}, parent, false)
                MyViewHolder1(layoutView)
            }
            else -> {
                val layoutView = LayoutInflater.from(parent.context).inflate(
                        R.layout.${LAYOUT_FILE}, parent, false)
                MyViewHolder2(layoutView)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as MyViewHolder1
        viewHolder.view.model = itemList[position]
        if (position > lastAdded) {
            viewHolder.view.inAnimation()
            lastAdded = position
        }
    }

    inner class MyViewHolder1(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ${VIEW} = itemView.findViewById(R.id.view)
        init {
            view.initialize(itemView)
        }
    }

    inner class MyViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view: ${VIEW} = itemView.findViewById(R.id.view)
        init {
            view.initialize(itemView)
        }
    }

    override fun getItemCount(): Int {
        return this.itemList.size
    }
}

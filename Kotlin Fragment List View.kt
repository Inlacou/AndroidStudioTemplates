#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.app.Activity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import timber.log.Timber

#parse("File Header.java")
class ${NAME} : BaseFrag() {

    //List
    @BindView(R.id.swiperefreshlayout) @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null
    @BindView(R.id.recyclerview) @JvmField var recyclerView: RecyclerView? = null
    @BindView(R.id.fab) @JvmField var fab: FloatingActionButton? = null
    @BindView(R.id.content) @JvmField var lContent: View? = null
    @BindView(R.id.empty) @JvmField var lEmpty: View? = null
	@BindView(R.id.loading) @JvmField var lLoading: View? = null
    ///List

    private lateinit var model: ${MODEL}
	private val controller: ${CONTROLLER} by lazy { baseController as ${CONTROLLER} }

    companion object {
		@JvmOverloads
        fun create(model: ${MODEL} = ${MODEL}()): ${NAME} {
            val fragment = ${NAME}()
            val args = Bundle()
            args.putString("model", Gson().toJson(model))
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Timber.d("onCreateView")

        val rootView = inflater.inflate(R.layout.fragment_list_news, container, false)

        getArgs()

        initialize(rootView, savedInstanceState)

        populate(rootView)

        setListeners()

        return rootView
    }

    fun getArgs(){
		arguments?.let {
        	if (it.containsKey("model")) model = Gson().fromJson<${MODEL}>(it.getString("model"))
        }
    }

    protected fun initialize(rootView: View, savedInstanceState: Bundle?) {
        super.initialize(rootView)
        baseController = ${CONTROLLER}(view = this, model = model)
    }

    private fun populate(rootView: View) {
    }

    private fun setListeners() {
        swipeRefreshLayout!!.setOnRefreshListener({ controller.onRefresh() })
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() { //Disable if multiple pages
			override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
				super.onScrolled(recyclerView, dx, dy)
				controller.onScrolled()
			}
		})
    }

    fun onEmpty() {
		lEmpty?.visibility = View.VISIBLE
		lLoading?.visibility = View.GONE
		lContent?.visibility = View.GONE
		setRefreshing(false)
	}

	fun onLoading() {
		lEmpty?.visibility = View.GONE
		lLoading?.visibility = View.VISIBLE
		lContent?.visibility = View.GONE
		setRefreshing(true)
	}

	fun onContent() {
		lEmpty?.visibility = View.GONE
		lLoading?.visibility = View.GONE
		lContent?.visibility = View.VISIBLE
		setRefreshing(false)
	}

    override fun onDestroyView() {
        super.onDestroyView()
		controller.onDestroy()
    }

    private fun setRefreshing(b: Boolean = true) {
        swipeRefreshLayout?.post { swipeRefreshLayout?.isRefreshing = b }
    }
    
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        controller.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
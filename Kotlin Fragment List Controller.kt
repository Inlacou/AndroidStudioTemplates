#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.bigshowi.user.adapters.rv.ChatRvAdapter
import com.bigshowi.user.ui.fragments.BaseFragCtrl

#parse("File Header.java")
class ${NAME}(val view: ${VIEW}, val model: ${MODEL}): BaseFragCtrl(view, model) {

    //List
    private var linearLayoutManager: LinearLayoutManager? = null
    private var adapter: ${ADAPTER}
    private var page: Int = 0
    private var loading = false
    ///List

    init {
        linearLayoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        view.recyclerView?.layoutManager = linearLayoutManager
        view.recyclerView?.isVerticalFadingEdgeEnabled = false
        view.recyclerView?.isHorizontalFadingEdgeEnabled = false
        adapter = ${ADAPTER}(itemList = model.itemList)
        view.recyclerView!!.adapter = adapter
        update()
    }

	private fun setLoading(b: Boolean) {
		loading = b
		if (b && model.itemList.isEmpty()) view.onLoading()
	}

    fun onRefresh() {
        update()
    }

    fun onUpdated(){
        if (model.itemList.size > 0) {
            view.onContent()
        } else {
            view.onEmpty()
        }
        adapter.notifyDataSetChanged()
		loading = false
    }

    private fun update() {
        page = 0
        model.itemList.clear()
        load()
    }

    private fun load() {
        if (loading) {
            return
        } else {
			setLoading(true)
            //TODO
			onUpdated()
        }
    }

    fun onScrolled() {
        linearLayoutManager?.let { linearLayoutManager: LinearLayoutManager ->
            view.recyclerView?.let { recyclerView: RecyclerView ->
                val visibleItemCount = recyclerView.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val firstVisibleItemIndex = linearLayoutManager.findFirstVisibleItemPosition()

                if (!loading) {
                    when {
                        totalItemCount - visibleItemCount <= firstVisibleItemIndex -> {
                            // Loading NOT in progress and end of list has been reached
                            // also triggered if not enough items to fill the screen
                            // if you start loading
                            loadMore()
                        }
                        firstVisibleItemIndex == -1 -> {
                            loadMore()
                        }
                        firstVisibleItemIndex == 0 -> {
                            // top of list reached
                            // if you start loading
                            loading = false
                        }
                    }
                }
            }
        }
    }
}
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end

#parse("File Header.java")
data class ${NAME} (val item: Any = Any(),
		val onClick: ((item: Any) -> Unit?)? = null,
		val onDelete: ((item: Any) -> Unit?)? = null)
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Intent
import android.support.v4.app.Fragment
import android.view.View

import com.afollestad.materialdialogs.MaterialDialog
import com.libraries.inlacou.volleycontroller.VolleyController

import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

import butterknife.ButterKnife
import butterknife.Unbinder
import io.reactivex.disposables.Disposable
import timber.log.Timber

#parse("File Header.java")
abstract class ${NAME} : Fragment() {

	protected lateinit var mInstance: ${NAME}
	protected var unbinder: Unbinder? = null
	protected lateinit var baseController: BaseFragCtrl
	var loadingDialog: MaterialDialog? = null
	var unknownErrorDialog: MaterialDialog? = null
	var disposables: ArrayList<Disposable> = ArrayList()

	protected fun initialize(rootView: View) {
		mInstance = this
		unbinder = ButterKnife.bind(mInstance, rootView)
		EventBus.getDefault().register(mInstance)
		mInstance.activity?.let {
			loadingDialog = MaterialDialog.Builder(it)
					.cancelable(false)
					.progress(true, 10)
					.content(R.string.Loading)
					.build()
			unknownErrorDialog = MaterialDialog.Builder(it)
					.cancelable(true)
					.title(R.string.Error)
					.content(R.string.Unknown_error_happened)
					.build()
		}
	}

	override fun onDestroyView() {
		loadingDialog?.dismiss()
		baseController.onDestroy()
		for (i in 0 until disposables.size) {
			disposables[i].dispose()
		}
		VolleyController.getInstance().cancelRequest(mInstance)
		EventBus.getDefault().unregister(mInstance)
		unbinder?.unbind()
		super.onDestroyView()
	}
	
	@Subscribe(threadMode = ThreadMode.MAIN)
	fun onAnyEvent(event: BaseEvent) {
		Timber.d(".on" + event.javaClass.name)
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		baseController.onRequestPermissionsResult(requestCode, permissions, grantResults)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		baseController.onActivityResult(requestCode, resultCode, data)
	}
}
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View

import com.google.gson.Gson

import butterknife.BindView
import com.github.salomonbrys.kotson.fromJson

#parse("File Header.java")
class ${NAME} : BaseActivity() {

    private lateinit var model: ${MODEL}
	private val controller: ${CONTROLLER} by lazy { baseController as ${CONTROLLER} }

    //List
    @BindView(R.id.swiperefreshlayout) @JvmField var swipeRefreshLayout: SwipeRefreshLayout? = null
    @BindView(R.id.recyclerview) @JvmField var recyclerView: RecyclerView? = null
    @BindView(R.id.fab) @JvmField var fab: FloatingActionButton? = null
    @BindView(R.id.empty) @JvmField var lEmpty: View? = null
	@BindView(R.id.loading) @JvmField var lLoading: View? = null
	@BindView(R.id.content) @JvmField var lContent: View? = null
    ///List

    companion object {
        fun navigate(activity: Activity, model: ${MODEL}) {
            val intent = Intent(activity, ${NAME}::class.java)

            intent.putExtra("model", Gson().toJson(model))

            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.${VIEW})

        getIntentData()

        initialize(savedInstanceState)

        configureActionBar()

        populate()

        setListeners()
    }

    private fun getIntentData() {
        if (intent.hasExtra("model")) model = Gson().fromJson<${MODEL}>(intent.extras.getString("model"))
    }

    private fun configureActionBar() {
		val toolbar = findViewById<Toolbar>(R.id.toolbar)
		if(toolbar!=null) {
			setSupportActionBar(toolbar)
			supportActionBar?.title = "TODO"
			supportActionBar?.setDisplayHomeAsUpEnabled(true)
		}
    }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        baseController = ${CONTROLLER}(view = this, model = model)
    }

    private fun populate() {
    }

    private fun setListeners() {
        swipeRefreshLayout!!.setOnRefreshListener({ controller.onRefresh() })
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                controller.onScrolled(recyclerView)
            }
        })
    }

    fun onEmpty(){
        lEmpty?.visibility = View.VISIBLE
        lLoading?.visibility = View.GONE
        lContent?.visibility = View.GONE
		setRefreshing(false)
    }
    
    fun onLoading() {
		lEmpty?.visibility = View.GONE
		lLoading?.visibility = View.VISIBLE
		lContent?.visibility = View.GONE
		setRefreshing(true)
	}
	
    fun onContent(){
        lEmpty?.visibility = View.GONE
        lLoading?.visibility = View.GONE
        lContent?.visibility = View.VISIBLE
		setRefreshing(false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.none, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // app icon in action bar clicked; goto parent activity.
                this.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        controller.onDestroy()
        super.onDestroy()
    }

    fun setRefreshing(b: Boolean = true) {
        swipeRefreshLayout?.post { swipeRefreshLayout!!.isRefreshing = b }
    }
}
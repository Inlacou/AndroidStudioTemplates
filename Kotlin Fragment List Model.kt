#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
#parse("File Header.java")
data class ${NAME} @JvmOverloads constructor(val itemList: MutableList<${ADAPTER_VIEW_MODEL}> = mutableListOf())
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson

#parse("File Header.java")
class ${NAME} : BaseAct() {
    private lateinit var model: ${MODEL}
	private val controller: ${CONTROLLER} by lazy { baseController as ${CONTROLLER} }

    companion object {
        fun navigate(activity: Activity, model: ${MODEL}) {
            val intent = Intent(activity, ${NAME}::class.java)

            intent.putExtra("model", Gson().toJson(model))

            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.${XML})

        getIntentData()

        initialize(savedInstanceState)

        configureActionBar()

        populate()

        setListeners()
    }

    private fun getIntentData() {
        if (intent.hasExtra("model")) model = Gson().fromJson(intent.extras.getString("model"))
    }

    private fun configureActionBar() {
		val toolbar = findViewById<Toolbar>(R.id.toolbar)
		if(toolbar!=null) {
			setSupportActionBar(toolbar)
			supportActionBar?.title = "TODO"
			supportActionBar?.setDisplayHomeAsUpEnabled(true)
		}
    }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        baseController = ${CONTROLLER}(view = this, model = model)
    }

    fun populate() {

    }

    private fun setListeners() {

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.none, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                // app icon in action bar clicked; goto parent activity.
                this.finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}

#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end

#parse("File Header.java")
class ${NAME}(val view: ${VIEW}, var model: ${MODEL}) {

    init { //Initialize
        
    }
    
    fun onClick(){
        model.onClick?.invoke(model.item)
    }
    
    fun onDelete(){
        model.onDelete?.invoke(model.item)
    }
}
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
#parse("File Header.java")
class ${NAME}(val view: ${VIEW}, val model: ${MODEL}): BaseFragCtrl(view, model)
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Intent
import android.os.Bundle
import timber.log.Timber
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson

import butterknife.ButterKnife

#parse("File Header.java")
class ${NAME} : BaseFrag() {

    companion object {
        fun create(model: ${MODEL} = ${MODEL}()): ${NAME} {
            val fragment = ${NAME}()
            val args = Bundle()
            args.putString("model", Gson().toJson(model))
            fragment.arguments = args
            return fragment
        }
    }
    
	private val controller: ${CONTROLLER} by lazy { baseController as ${CONTROLLER} }
    private lateinit var model: ${MODEL}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Timber.d("onCreateView")

        val rootView = inflater.inflate(R.layout.${LAYOUT}, container, false)

        getArgs()
        
        initialize(rootView, savedInstanceState)

        populate(rootView)

        setListeners()

        return rootView
    }

	private fun getArgs() {
        arguments?.let {
            if (it.containsKey("model")) model = Gson().fromJson<${MODEL}>(it.getString("model"))
        }
        
    }

    protected fun initialize(rootView: View, savedInstanceState: Bundle?) {
        super.initialize(rootView)
        baseController = ${CONTROLLER}(view = this, model = model)
    }

    fun populate(rootView: View? = null) {}

    private fun setListeners() {

    }
}
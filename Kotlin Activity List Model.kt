#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end

#parse("File Header.java")
data class ${NAME}(val itemList: MutableList<${LIST_ELEMENT_MODEL}> = mutableListOf())
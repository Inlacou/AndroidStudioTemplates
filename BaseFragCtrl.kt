#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Intent

#parse("File Header.java")
abstract class ${NAME}(val _view: BaseFrag, val _model: Any) {
	
	init { //initialize
	}
	
	open fun onDestroy() {
	}
	
	open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
	}
	
	open fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
	}
}
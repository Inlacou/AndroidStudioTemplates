#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Intent
import com.github.salomonbrys.kotson.fromJson
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import timber.log.Timber
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

#parse("File Header.java")
class ${NAME}(val view: ${VIEW}, val model: ${MODEL}): BaseActCtrl(view, model) {

    //List
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: ${ADAPTER}? = null
    private var page: Int = 0
    private var loading = false
    ///List

    init {
        EventBus.getDefault().register(this)
        layoutManager = LinearLayoutManager(view, LinearLayoutManager.VERTICAL, false)
        view.recyclerView?.layoutManager = layoutManager
        view.recyclerView?.isVerticalFadingEdgeEnabled = false
        view.recyclerView?.isHorizontalFadingEdgeEnabled = false
        adapter = ${ADAPTER}(itemList = model.itemList)
        view.recyclerView!!.adapter = adapter
        update()
    }

	private fun setLoading(b: Boolean) {
		loading = b
		view.setRefreshing(b)
		if (b && model.itemList.isEmpty()) view.onLoading()
	}

    fun onRefresh() {
        update()
    }

    fun onScrolled(recyclerView: RecyclerView?) {
        val visibleItemCount = recyclerView!!.childCount
        val totalItemCount = layoutManager!!.itemCount
        val firstVisibleItemIndex = layoutManager!!.findFirstVisibleItemPosition()

        //synchronize loading state when item count changes
        /*if (loading) {
            if (totalItemCount > drivers.size()) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }*/
        if (!loading) {
            if (totalItemCount - visibleItemCount <= firstVisibleItemIndex) {
                // Loading NOT in progress and end of list has been reached
                // also triggered if not enough items to fill the screen
                // if you start loading
                load()
            } else if (firstVisibleItemIndex == -1) {
                load()
            } else if (firstVisibleItemIndex == 0) {
                // top of list reached
                // if you start loading
                loading = false
                //Log.d(DEBUG_TAG, "loading " + loading);
            }
        }
    }

    fun onUpdated(){
        if(model.itemList.size>0){
            view.onContent()
        }else{
            view.onEmpty()
        }
        setLoading(false)
        adapter?.notifyDataSetChanged()
    }

    private fun update() {
        page = 0
		model.itemList.clear()
        load()
    }

    private fun load() {
        if (loading) {
            return
        } else {
            setLoading(true)
            //TODO get list
        }
    }

    override fun onDestroy() {
		EventBus.getDefault().unregister(this)
		super.onDestroy()
	}

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAnyEvent(event: BaseEvent) {
		Timber.d(".on${event.javaClass.name}")
    }
}
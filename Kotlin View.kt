#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout

import butterknife.BindView
import butterknife.ButterKnife

#parse("File Header.java")
class ${NAME} @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
	: FrameLayout(context, attrs, defStyleAttr) {

    @BindView(R.id.view_base_layout_surface) @JvmField internal var surfaceLayout: View? = null
    
    var model: ${MODEL} = ${MODEL}()
		set(value) {
			field = value
			controller.model = value
			populate()
		}
    private lateinit var controller: ${CONTROLLER}

    init {
        this.initialize()
        setListeners()
        populate()
    }

    protected fun initialize() {
        val rootView = View.inflate(context, R.layout.${LAYOUT}, this)
        initialize(rootView)
    }

    fun initialize(view: View) {
		controller = ${CONTROLLER}(view = this, model = model)
        ButterKnife.bind(this, view)
    }

    fun populate() {

    }

    private fun setListeners() {
        surfaceLayout?.setOnClickListener { controller.onClick() }
    }

    fun inAnimation() {
        val animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        surfaceLayout?.startAnimation(animation)
    }

    fun outAnimation() {
        val animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        surfaceLayout?.startAnimation(animation)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                controller.onDelete()
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })
    }
}